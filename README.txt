Script d'installation automatisée de GNS3 et ElasticSearch sur Debian 11

Ce script permet d'installer automatiquement la dernière version de GNS3 et ElasticSearch sur un système Debian 11.

Prérequis:

    Avoir les privilèges administrateur sur le système (sudo ou root).
    Avoir un accès internet.

Utilisation:

    Téléchargez le script sur le système cible :

curl "liens vers notre script >> install_gns3_elastic.sh"

    Donner les droits d'exécution au script:

chmod +x install_gns3_elastic.sh

    Exécuter le script en tant qu'utilisateur administrateur :

sudo ./install_gns3_elastic.sh

 Il est conseillé de tester ce script en premier lieu sur un environnement de test avant de l'utiliser en production ;)

Liens utiles: 

    Site web de GNS3 : https://gns3.com/
    Documentation de GNS3 : https://docs.gns3.com/docs/
    Dépôt Gitlab du script : ...

#TeamProjet5 
